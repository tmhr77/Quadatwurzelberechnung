#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <conio.h>

double wurzel(double a){

	// Variablen deklarieren
	 int i;
	 double x;

	// Startwert festlegen
	x = a / 3;

	// Algorithmus zur näherungsweisen Berechnung der Quadratwurzel
	for (i = 1; i <= 20; ++i) {
	      x = (x + a / x) / 2;
	   	   }

	// Ergebnis zurückgeben
	return x;
}


int main(void){

	//  Variable zur Eingabe der Zahl deklarieren
	int z;

	// Zahl einlesen
	printf("Bitte geben Sie die Zahl ein, von der die Quadratwurzel geschaetzt werden soll:");
	scanf("%d",&z);

	// Ergebnis ausgeben
	printf("\nDie berechnete Quadratwurzel von %d lautet: %f\n",z, wurzel(z));
	printf("(Zum Vergleich sqrt(%d): %f) \n\n",z, sqrt(z));

	// sqrt(2) und sqrt(16) testen
	printf("Test des Algorithmus  mit sqrt(2) und sqrt(16):\n");
	if (sqrt(2) == wurzel(2)){
			printf("sqrt(2): %f und wurzel(2): %f\n",sqrt(2),wurzel(2));
			printf("sqrt(16): %f und wurzel(16): %f\n\n",sqrt(16), wurzel(16));
	}else
			{printf("ERROR - der Algorithmus wurde nicht korrekt eingelesen.");}


	return 0; 
	getch();

}
 