------------------ QUADRATWURZELBERECHNUNG - README --------------------------

Inhalt dieses Projektes ist die Umsetzung einer Quadratwurzelberechnung in C.
Die Quadratwurzelberechnung wird mithilfe des Näherungsverfahren (auch bekannt
als Heronverfahren) umgesetzt und mit sqrt(2) und sqrt(16) getestet.
Da das Hauptaugenmerk dieses Verfahrens auf einer Iteration liegt, wird dies
sehr wahrscheinlich die größte technische Herausforderung sein.