----------------- QUADRATWURZELBERECHNUNG - CHANGELOG -------------------------

18.12.2015 - Erstellung von README.md, CONTRIBUTING.md und CHANGELOG.md

28.12.2015 - Rohfassung des Programms erstellt. Folgende Funktionen sind enthalten:
             - Näherungsweise Berechnung von Quadratwurzeln mithilfe von Iteration in einer Funktion
             - Ein- und Ausgabefunktion in der main-Funktion
             - Test mit sqrt(2) und sqrt(6)
             - Test mit sqrt() der eingegebenen Zahl
             - Kommentare
             
09.01.2016 - Finale Version des Programms:
             - Algorithmus optimiert
             - Vergleich mit sqrt()
             - diverse Kommentarerweiterungen und Verbesserungen
             - Formatierung des Codes